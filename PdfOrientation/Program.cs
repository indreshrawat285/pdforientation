﻿/*******************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 * 
 * Project:       PdfOrientation
 * File:          Program.cs
 *
 * Purpose:
 *      Run the Application.
 *  
 * Revision History: 28-06-2020 File Created (IVNR)
 *******************************************************************************/

using System;

namespace PdfOrientation
{
    class Program
    {
        static void Main(string[] args)
        {

            // Initializing the sourcePath with the path of pdf to
            // be read.
            string sourcePath = @"..\..\source\samplepdf.pdf";

            // Initializing the destinationPath with the path of pdf to
            // be generated.
            string destinationPath = @"..\..\destination\landscapeoutput";

            // Instantiation the pdfManipulation object.
            PdfManipulation pdfManipulation = new PdfManipulation();

            // Calling the RotatePdfToLandascape to rotate the pdf in landscape.
            pdfManipulation.RotatePdfToLandscape(sourcePath, destinationPath);

            // Reading the console to keep it open.
            Console.Read();
        }
    }
}
