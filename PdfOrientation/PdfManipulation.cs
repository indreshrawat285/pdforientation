﻿
/*******************************************************************************
 * Author:        Indresh Rawat (IVNR)
 * Email:         indreshrawat.gitlab@gmail.com
 * 
 * Project:       PdfOrientation
 * File:          PdfManipulation.cs
 *  
 * Purpose:
 *      Allows mainpulation on Pdf files.
 *  
 * Revision History: 28-06-2020 (IVNR) File Created.
 *                   29-06-2020 (IVNR) Updated RotatePdf method.
 *******************************************************************************/

using System;
using System.IO;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;

namespace PdfOrientation
{
    /// <summary>
    /// PdfManipulation class is used to do operations on the Pdf.
    /// </summary>
    public class PdfManipulation
    {

        /// <summary>
        /// The below method will read the pdf from source which is in potairt mode and rotate the pdf to 
        /// landscape and store in the destination as a new pdf.
        /// </summary>
        /// <param name="sourcePath">is the path of the pdf to read.</param>
        /// <param name="destinationPath">is the path of the pdf to write.</param>
        public void RotatePdfToLandscape(string sourcePath, string destinationPath)
        {

            // Defining a string variable which will returned the newpathGenerated string from
            // the method and as well create the pdf file.
            string newGeneratedFilePath = CreateFileAndReturnedPath(destinationPath);

            // Calling the method to read the pdf from the sourcePath then rotate the pdf and
            // store it in the newGeneratedFilePath
            RotatePdf(sourcePath, newGeneratedFilePath);
        }

        /// <summary>
        /// The below method will create the pdf with the name in the format: 
        /// filename + D + dd-MM-yyyy + T + HH-mm-ss +.pdf and return the path to
        /// the caller of the function.
        /// </summary>
        /// <param name="destinationPath">is the path of the pdf to write.</param>
        /// <returns>Returns the path of the pdf.</returns>
        private string CreateFileAndReturnedPath(string destinationPath)
        {

            // Retrieving the current DateTime.
            string currentDateTime = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");

            // Rplacing ':' with '-' for time, since ':' is not allowed for file creation.
            currentDateTime = currentDateTime.Replace(":", "-");

            // Apending 'D' for Date and replace ' ' with 'T' to showcase they are 
            // date and time in currentDateTime.
            currentDateTime = "D" + currentDateTime.Replace(" ", "T");

            // Initializing the fileExtension with .pdf.
            string fileExtension = ".pdf";

            // The newGeneratedFilePath is created with currentDateTime appended
            // to the destinationPath.
            string newGeneratedFilePath = destinationPath + currentDateTime + fileExtension;

            // Creating the file.
            FileStream fileStream = File.Create(newGeneratedFilePath);

            // Closing the fileSream.
            fileStream.Close();

            // Returning the newGeneratedPath to the caller.
            return newGeneratedFilePath;
        }

        /// <summary>
        /// The below method will read the pdf from source and will rotate the pages to landscape and
        /// write in the destination pdf.
        /// </summary>
        /// <param name="sourcePath">is the path of the pdf to read.</param>
        /// <param name="destinationPath">is the path of the pdf to write.</param>
        private void RotatePdf(string sourcePath, string destinationPath)
        {

            // Reading the pdf from the filepath.
            PdfReader pdfReader = new PdfReader(sourcePath);

            // Writing the pdf from the destinationPath.
            PdfWriter pdfWriter = new PdfWriter(destinationPath);

            // Opening the pdfDocument in stamping mode.
            PdfDocument pdfDocument = new PdfDocument(pdfReader, pdfWriter);

            // Itearating through the pages of the pdf.
            for (int pageNumber = 1; pageNumber <= pdfDocument.GetNumberOfPages(); pageNumber++)
            {

                // Retrieving the page from the document.
                PdfPage page = pdfDocument.GetPage(pageNumber);

                // Retrieving the boundary of the page.
                Rectangle media = page.GetCropBox();

                // Checking if the media is null.
                if (null == media)
                {

                    // Retieving the boundary of the page.
                    media = page.GetMediaBox();
                }

                // Retrieving the width of the rectangle.
                float widthOfTheRectangle = media.GetWidth();

                // Retrieving the hieght of the rectangle.
                float hieghtOfTheRectangle = media.GetHeight();

                // Making the page to feel like landscape by setting it's boundary.
                Rectangle crop = new Rectangle(0, 0, widthOfTheRectangle * 1.27f, hieghtOfTheRectangle / 1.75f);

                // Boundary of the page.
                page.SetMediaBox(crop);

                // The visible part of the page to the user.
                page.SetCropBox(crop);
                
                // The content, placed on a content stream before, will be rendered before the other content
                // and, therefore, could be understood as a background (bottom "layer")
                new PdfCanvas(page.NewContentStreamBefore(),
                        page.GetResources(), pdfDocument).WriteLiteral("\nq 1.2 0 0 0.52 20 20 cm\nq\n");
                                                                         // width x y hieght left pad bottom pad
                // The content, placed on a content stream after, will be rendered after the other content
                // and, therefore, could be understood as a foreground (top "layer")
                new PdfCanvas(page.NewContentStreamAfter(),
                        page.GetResources(), pdfDocument).WriteLiteral("\nQ\nQ\n");
            }

            // Closing the pdf Document.
            pdfDocument.Close();
        }
    }
}


